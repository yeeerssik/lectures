<?php

namespace App\Repositories;

class BaseRepository implements IRecipeRepository {

    protected $model;
    public function all()
    {
        return $this->model::all();
    }

    public function find($id)
    {
        return $this->model::findOrFail($id);
    }

    public function create($data)
    {
        return $this->model::create($data);
    }

    public function update($data, $id)
    {
        return $this->model::findOrFail($id)->update($data->all());
    }

    public function delete($id)
    {
        return $this->model::findOrFail($id)->delete();
    }
}
