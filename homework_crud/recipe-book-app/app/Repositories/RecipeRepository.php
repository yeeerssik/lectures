<?php

namespace App\Repositories;

use App\Models\Recipe;

class RecipeRepository extends BaseRepository implements IRecipeRepository {

    public function all()
    {
        return Recipe::all();
    }

    public function find($id)
    {
        return Recipe::findOrFail($id);
    }

    public function create($data)
    {
        return Recipe::create([
            'title' => $data->title,
            'category_id' => $data->category_id,
            'description' => $data->description,
            'img_path' => $data->img_path,
        ]);
    }

    public function update($data, $id)
    {
        return Recipe::findOrFail($id)->update($data->all());
    }

    public function delete($id)
    {
        return Recipe::findOrFail($id)->delete();
    }
}
