<?php

namespace App\Http\Controllers\Api;

use AllowDynamicProperties;
use App\Exports\RecipesExport;
use App\Http\Requests\UpdateRecipeRequest;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\StoreRecipeRequest;
use App\Services\RecipeService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Recipe-app Swagger API",
 *      description="L5 Swagger OpenApi description for Recipe-app",
 *      @OA\Contact(
 *          email="ersultan.valihan@gmail.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 */
#[AllowDynamicProperties] class RecipeController extends BaseController
{
    public function __construct(RecipeService $recipeService) {
        $this->recipeService = $recipeService;
    }

    /**
     * @OA\Get(
     *     path="/api/recipes",
     *     summary="Get list of recipes",
     *     @OA\Response(
     *         response="200",
     *         description="Success.",
     *         @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/Recipe")
     *         )
     *     )
     * )
     */
    public function index()
    {
        return $this->recipeService->all();
    }

    /**
     * @OA\Post(
     *     path="/api/recipes",
     *     summary="Create a new recipe",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  schema="Recipe",
     *                  type="object",
     *                  title="Recipe",
     *                  required={"title", "category_id", "description"},
     *                  @OA\Property(
     *                      property="title",
     *                      type="string",
     *                      description="Title"
     *                  ),
     *                  @OA\Property(
     *                      property="category_id",
     *                      type="integer",
     *                      format="int64",
     *                      description="Category ID"
     *                  ),
     *                  @OA\Property(
     *                      property="description",
     *                      type="string",
     *                      format="string",
     *                      description="Description"
     *                  ),
     *                  @OA\Property(
     *                      property="user_id",
     *                      type="integer",
     *                      format="int64",
     *                      description="User ID"
     *                  ),
     *                  example={
     *                      "title": "Hamburger",
     *                      "category_id": 5,
     *                      "description": "Beef patty with sauce between two buns",
     *                      "user_id": 1
     *                  }
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Recipe created"
     *     )
     * )
     */
    public function store(StoreRecipeRequest|FormRequest $request)
    {
        return $this->recipeService->create($request);
    }

    /**
     * @OA\Put(
     *     path="/api/recipes/{id}",
     *     summary="Update recipe by ID",
     *     @OA\Parameter(
     *           in="path",
     *           name="id",
     *           required=true,
     *           @OA\Schema(type="integer"),
     *           @OA\Examples(example="int", value="1", summary="An int value.")
     *     ),
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *               mediaType="application/json",
     *               @OA\Schema(
     *                   schema="Recipe",
     *                   type="object",
     *                   title="Recipe",
     *                   @OA\Property(
     *                       property="title",
     *                       type="string",
     *                       description="Title"
     *                   ),
     *                   @OA\Property(
     *                       property="category_id",
     *                       type="integer",
     *                       format="int64",
     *                       description="Category ID"
     *                   ),
     *                   @OA\Property(
     *                       property="description",
     *                       type="string",
     *                       format="string",
     *                       description="Description"
     *                   ),
     *                   @OA\Property(
     *                       property="user_id",
     *                       type="integer",
     *                       format="int64",
     *                       description="User ID"
     *                   ),
     *                   example={
     *                       "title": "Hamburger",
     *                       "category_id": 5,
     *                       "description": "Beef patty with sauce between two buns",
     *                       "user_id": 1
     *                   }
     *               )
     *           )
     *      ),
     *     @OA\Response(response="200", description="Update recipe by ID.")
     * )
     */
    public function update(UpdateRecipeRequest|FormRequest $request, $id)
    {
        return $this->recipeService->update($request, $id);
    }

    /**
     * @OA\Delete(
     *     path="/api/recipes/{id}",
     *     summary="Delete recipe by ID",
     *     @OA\Parameter(
     *            in="path",
     *            name="id",
     *            required=true,
     *            @OA\Schema(type="integer"),
     *            @OA\Examples(example="int", value="1", summary="An int value.")
     *      ),
     *     @OA\Response(response="200", description="Delete recipe by ID.")
     * )
     */
    public function destroy($id)
    {
        return $this->recipeService->delete($id);
    }

    /**
     * @OA\Get(
     *     path="/api/recipes/{id}",
     *     summary="Show one recipe by ID",
     *     @OA\Parameter(
     *            in="path",
     *            name="id",
     *            required=true,
     *            @OA\Schema(type="integer"),
     *            @OA\Examples(example="int", value="1", summary="An int value.")
     *      ),
     *     @OA\Response(response="200", description="Show recipe by ID.")
     * )
     */
    public function show($id) {
        return $this->recipeService->find($id);
    }

    public function export() {
        return Excel::download(new RecipesExport, 'recipes.xlsx');
    }
}
