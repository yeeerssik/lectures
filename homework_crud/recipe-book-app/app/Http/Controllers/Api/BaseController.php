<?php

namespace app\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;

class BaseController extends Controller
{
    protected $service;

    public function index() {
        return $this->service->all();
    }
    public function store(FormRequest $request) {
        return $this->service->create($request);
    }
    public function update(FormRequest $request, $id) {
        return $this->service->update($request, $id);
    }
    public function destroy($id) {
        return $this->service->delete($id);
    }
    public function show($id) {
        return $this->service->find($id);
    }
}
