<?php

namespace App\Http\Controllers;

use App\Exports\RecipesExport;
use App\Http\Requests\StoreRecipeRequest;
use App\Http\Requests\UpdateRecipeRequest;
use App\Models\Recipe;
use App\Services\RecipeService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class RecipeController extends Controller
{
    public function __construct(protected RecipeService $recipeService) {}

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $recipes = $this->recipeService->all();
        return view('recipes.index', compact('recipes'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRecipeRequest|FormRequest $request)
    {
        $this->recipeService->create($request);
        return redirect()->route('recipes.index')
            ->with('success', 'Recipe created!');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRecipeRequest|FormRequest $request, string $id)
    {
        $this->recipeService->update($request, $id);
        return redirect()->route('recipes.index')
            ->with('success', 'Recipe updated!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->recipeService->delete($id);
        return redirect()->route('recipes.index')
            ->with('success', 'Recipe deleted!');
    }

    public function create()
    {
        return view('recipes.create');
    }

    public function show($id) {
        $recipe = Recipe::find($id);
        return view('recipes.show', compact('recipe'));
    }

    public function edit($id) {
        $recipe = Recipe::find($id);
        return view('recipes.edit', compact('recipe'));
    }

    public function export() {
        return Excel::download(new RecipesExport, 'recipes.xlsx');
    }
}
