<?php

namespace app\Providers;

use app\Services\RecipeService;
use Illuminate\Support\ServiceProvider;
use App\Repositories\IRecipeRepository;
use App\Repositories\RecipeRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(IRecipeRepository::class, RecipeRepository::class);
        $this->app->bind(RecipeService::class, function ($app) {
           return new RecipeService($app->make(IRecipeRepository::class));
        });
//        $this->app->bind(
//            'App\Repositories\IRecipeRepository',
//            'App\Repositories\RecipeRepository'
//        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
