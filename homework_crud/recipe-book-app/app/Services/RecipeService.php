<?php

namespace App\Services;

use App\Repositories\IRecipeRepository;

class RecipeService extends BaseService
{
    public IRecipeRepository $recipeRepository;
    public function __construct(IRecipeRepository $recipeRepository) {
        $this->recipeRepository = $recipeRepository;
    }
    public function all() {
        return $this ->recipeRepository->all();
    }
    public function find($id) {
        return $this->recipeRepository->find($id);
    }
    public function create($data) {
        return $this->recipeRepository->create($data);
    }
    public function update($data, $id) {
        return $this->recipeRepository->update($data, $id);
    }
    public function delete($id) {
        return $this->recipeRepository->delete($id);
    }
}
