import ExampleComponent from './components/ExampleComponent.vue';
import ContactComponent from './components/ContactComponent.vue';
import RecipesComponent from "./components/RecipesComponent.vue";
import RecipeDetailComponent from "./components/RecipeDetailComponent.vue";

const routes = [
    { path: '/', component: ExampleComponent },
    { path: '/recipes', component: RecipesComponent },
    { path: '/recipes/:id', component: RecipeDetailComponent, props: true, name: 'recipe-detail' },
    { path: '/contact', component: ContactComponent },
];

export default routes;
