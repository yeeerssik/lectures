<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecipeController;

Route::resource('recipe', RecipeController::class);


Route::get('/', function() {
   return view('welcome') ;
});

//Route::get('/{any}', function () {
//    return view('layouts.client');
//})->where('any', '.*');

Route::group(['prefix'=>'admin'], function() {
    Route::group(['prefix'=>'recipes'], function() {
        Route::get('/', RecipeController::class . '@index')->name('recipes.index');
        Route::get('/create', RecipeController::class . '@create')->name('recipes.create');
        Route::get('/export', RecipeController::class . '@export')->name('recipes.export');
        Route::post('/', RecipeController::class . '@store')->name('recipes.store');
        Route::get('/{recipe}', RecipeController::class . '@show')->name('recipes.show');
        Route::get('/{recipe}/edit', RecipeController::class . '@edit')->name('recipes.edit');
        Route::put('/{recipe}', RecipeController::class . '@update')->name('recipes.update');
        Route::delete('/{recipe}', RecipeController::class . '@destroy')->name('recipes.destroy');
    });
});
