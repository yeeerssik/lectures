/*

1. Дано число. Проверьте, отрицательное оно или нет. Выведите об этом информацию в консоль

2. Дана строка. Выведите в консоль длину этой строки.

3. Дана строка. Проверьте, есть ли в этой строке символ 'a'. Если есть, то выведите 'да', если нет, то 'нет'.

4. Дано число. Проверьте, что оно делится на 2, на 3, на 5, на 6, на 9 без остатка.

5. Дано число. Проверьте, что оно делится на 3, на 5, на 7, на 11 без остатка.

6. Дана строка. Выведите в консоль последний символ строки.

7. Дана строка. Выведите в консоль последний символ строки.

8. Дан треугольник. Выведите в консоль его площадь.

9. Дан прямоугольник. Выведите в консоль его площадь.

10. Дано число. Выведите в консоль квадрат этого числа.

*/

<?php

// #1
function IsNegativeNumber($number) {
    if ($number < 0) {
        echo "{$number} is negative";
    } else if ($number > 0) {
        echo "{$number} is positive";
    }
}

// #2
function GetLenOfString($string) {
    echo strlen($string);
}

// #3
function IsIncludeA($string) {
    $flag = false;
    foreach ($string as &$value) {
        if ($value == 'a') {
            $flag = true;
            break;
        }
    }
    if ($flag) {
        echo "да";
    } else {
        echo "нет";
    }
}

// #4 - #5
function IsDivideWithoutRemainder($number) {
    if ($number % 2 == 0
        && $number % 3 == 0
        && $number % 5 == 0
        && $number % 6 == 0
        && $number % 9 == 0) {
        echo "Number is divide by 2,3,5,6,9";
    } else if ($number % 3 == 0
        && $number % 5 == 0
        && $number % 7 == 0
        && $number % 11 == 0) {
        echo "Number is divide by 3,5,7,11";
    }
}

// #6 - #7
function GetLastElement($string) {
    echo $string[-1];
}

// #8
function CalculateTriangleArea($a, $h) {
    $area = $a * $h * 0.5;

    echo $area;
}

// #9
function CalculateRectangleArea($a, $b) {
    $area = $a * $b;

    echo $area;
}

// #10
function GetSquareNumber($number) {
    echo $number ** 2;
}