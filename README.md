Лекции по курсу PHP Laravel
===========================
Лекции с примерами в папках `lecture1`, `lecture2`, `lecture3` и т.д.

Домашние задания в папках `homework1`, `homework2`, `homework3` и т.д.

Ссылка на проект 1 недели 2 - [todo-app](https://gitlab.com/yersssik/todo-app-php/ )

Ссылка на проект 2 недели 3, 4, 5 - [recipe-book](https://gitlab.com/yeeerssik/lectures/-/tree/crud-project?ref_type=heads)